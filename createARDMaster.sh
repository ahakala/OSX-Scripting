#!/bin/sh
#create home directory
mkdir /var/ardmaster
dscl . -create /Users/ardmaster
dscl . -create /Users/ardmaster IsHidden 1
dscl . -create /Users/ardmaster UserShell /bin/bash
dscl . -create /Users/ardmaster RealName "TIES ARD"
dscl . -create /Users/ardmaster UniqueID 301
dscl . -create /Users/ardmaster dscl . create /Users/ardmaster NFSHomeDirectory /var/ardmaster
dscl . -passwd /Users/ardmaster PASSWORD
dscl . -append /Groups/admin GroupMembership ardmaster